set ProjectRoot=C:\tmp\avr\HelloWorld2
set ProjectBuild=Z:\tmp\HelloWorld\Build
set ToolsRoot=C:\tmp\avr\ToolChain
set PyScripts=%ToolsRoot%\PyScripts
set AvrGcc=%ToolsRoot%\avr8-gnu-toolchain-win32_x86
set AvrDude=%ToolsRoot%\avrdude-6.3-mingw32
set DotNetLib=%ToolsRoot%\DotNetLib
C:\Python37\python.exe %PyScripts%\GenerateMake.py %ProjectRoot% %ProjectBuild% elf
pushd %ProjectBuild%
call make.exe -f "%ProjectBuild%\Makefile" flash
popd