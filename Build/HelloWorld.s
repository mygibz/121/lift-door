	.file	"HelloWorld.c"
__SP_H__ = 0x3e
__SP_L__ = 0x3d
__SREG__ = 0x3f
__tmp_reg__ = 0
__zero_reg__ = 1
	.text
.Ltext0:
	.cfi_sections	.debug_frame
	.section	.text.InitialState,"ax",@progbits
.global	InitialState
	.type	InitialState, @function
InitialState:
.LFB12:
	.file 1 "C:\\Users\\sgart\\Documents\\Coding\\C\\cpp\\lift-door\\Build\\HelloWorld.c"
	.loc 1 87 0
	.cfi_startproc
.LVL0:
/* prologue: function */
/* frame size = 0 */
/* stack size = 0 */
.L__stack_usage = 0
	ret
	.cfi_endproc
.LFE12:
	.size	InitialState, .-InitialState
	.section	.text.doorClosed,"ax",@progbits
.global	doorClosed
	.type	doorClosed, @function
doorClosed:
.LFB9:
	.loc 1 63 0
	.cfi_startproc
/* prologue: function */
/* frame size = 0 */
/* stack size = 0 */
.L__stack_usage = 0
	.loc 1 64 0
	ldi r18,lo8(2)
	ldi r20,lo8(1)
	ldi r22,lo8(3)
	ldi r24,lo8(2)
	call SendMessage
.LVL1:
	.loc 1 67 0
	out 0x27,__zero_reg__
	.loc 1 68 0
	out 0x28,__zero_reg__
	ret
	.cfi_endproc
.LFE9:
	.size	doorClosed, .-doorClosed
	.section	.text.doorOpened,"ax",@progbits
.global	doorOpened
	.type	doorOpened, @function
doorOpened:
.LFB10:
	.loc 1 70 0
	.cfi_startproc
/* prologue: function */
/* frame size = 0 */
/* stack size = 0 */
.L__stack_usage = 0
	.loc 1 71 0
	ldi r18,lo8(2)
	ldi r20,lo8(1)
	ldi r22,lo8(4)
	ldi r24,lo8(2)
	call SendMessage
.LVL2:
	.loc 1 74 0
	out 0x27,__zero_reg__
	.loc 1 75 0
	out 0x28,__zero_reg__
	ret
	.cfi_endproc
.LFE10:
	.size	doorOpened, .-doorOpened
	.section	.text.main,"ax",@progbits
.global	main
	.type	main, @function
main:
.LFB6:
	.loc 1 18 0
	.cfi_startproc
/* prologue: function */
/* frame size = 0 */
/* stack size = 0 */
.L__stack_usage = 0
	.loc 1 19 0
	ldi r22,lo8(-112)
	ldi r23,lo8(-48)
	ldi r24,lo8(3)
	ldi r25,0
	call Usart_Init
.LVL3:
	.loc 1 22 0
	ldi r24,lo8(controller)
	ldi r25,hi8(controller)
	call RegisterFsm
.LVL4:
	.loc 1 24 0
	ldi r24,lo8(gs(CustomAvrMessageHandler))
	ldi r25,hi8(gs(CustomAvrMessageHandler))
	call RegisterAvrMessageHandler
.LVL5:
	.loc 1 27 0
	ldi r20,lo8(gs(doorOpened))
	ldi r21,hi8(gs(doorOpened))
	ldi r22,lo8(3)
	ldi r24,0
	call RegisterExternalInteruptHandler
.LVL6:
	.loc 1 28 0
	ldi r20,lo8(gs(doorClosed))
	ldi r21,hi8(gs(doorClosed))
	ldi r22,lo8(3)
	ldi r24,lo8(1)
	call RegisterExternalInteruptHandler
.LVL7:
	.loc 1 31 0
	ldi r24,lo8(-95)
	out 0x24,r24
	.loc 1 32 0
	ldi r24,lo8(5)
	out 0x25,r24
	.loc 1 33 0
	out 0x27,__zero_reg__
	.loc 1 34 0
	out 0x28,__zero_reg__
	.loc 1 36 0
	call InitializeStateEventFramework
.LVL8:
	.loc 1 38 0
	ldi r24,0
	ldi r25,0
	ret
	.cfi_endproc
.LFE6:
	.size	main, .-main
	.section	.text.openDoor,"ax",@progbits
.global	openDoor
	.type	openDoor, @function
openDoor:
.LFB7:
	.loc 1 41 0
	.cfi_startproc
/* prologue: function */
/* frame size = 0 */
/* stack size = 0 */
.L__stack_usage = 0
	.loc 1 42 0
	ldi r18,lo8(2)
	ldi r20,lo8(1)
	ldi r22,lo8(1)
	ldi r24,lo8(3)
	call SendMessage
.LVL9:
	.loc 1 45 0
	ldi r22,0
	ldi r24,0
	call SetExtInterruptEnable
.LVL10:
	.loc 1 46 0
	ldi r22,lo8(1)
	ldi r24,lo8(1)
	call SetExtInterruptEnable
.LVL11:
	.loc 1 48 0
	ldi r24,lo8(51)
	out 0x27,r24
	.loc 1 49 0
	out 0x28,__zero_reg__
	ret
	.cfi_endproc
.LFE7:
	.size	openDoor, .-openDoor
	.section	.text.closeDoor,"ax",@progbits
.global	closeDoor
	.type	closeDoor, @function
closeDoor:
.LFB8:
	.loc 1 51 0
	.cfi_startproc
/* prologue: function */
/* frame size = 0 */
/* stack size = 0 */
.L__stack_usage = 0
	.loc 1 52 0
	ldi r18,lo8(2)
	ldi r20,lo8(1)
	ldi r22,lo8(2)
	ldi r24,lo8(3)
	call SendMessage
.LVL12:
	.loc 1 55 0
	ldi r22,lo8(1)
	ldi r24,0
	call SetExtInterruptEnable
.LVL13:
	.loc 1 56 0
	ldi r22,0
	ldi r24,lo8(1)
	call SetExtInterruptEnable
.LVL14:
	.loc 1 58 0
	out 0x27,__zero_reg__
	.loc 1 59 0
	ldi r24,lo8(51)
	out 0x28,r24
	ret
	.cfi_endproc
.LFE8:
	.size	closeDoor, .-closeDoor
	.section	.text.CustomAvrMessageHandler,"ax",@progbits
.global	CustomAvrMessageHandler
	.type	CustomAvrMessageHandler, @function
CustomAvrMessageHandler:
.LFB11:
	.loc 1 78 0
	.cfi_startproc
.LVL15:
	push r28
.LCFI0:
	.cfi_def_cfa_offset 3
	.cfi_offset 28, -2
	push r29
.LCFI1:
	.cfi_def_cfa_offset 4
	.cfi_offset 29, -3
/* prologue: function */
/* frame size = 0 */
/* stack size = 2 */
.L__stack_usage = 2
	movw r28,r24
	.loc 1 81 0
	ldd r24,Y+2
.LVL16:
	cpi r24,lo8(-49)
	brne .L8
	.loc 1 81 0 is_stmt 0 discriminator 1
	call openDoor
.LVL17:
.L8:
	.loc 1 82 0 is_stmt 1
	ldd r24,Y+2
	cpi r24,lo8(-17)
	brne .L9
	.loc 1 82 0 is_stmt 0 discriminator 1
	call closeDoor
.LVL18:
.L9:
	.loc 1 85 0 is_stmt 1
	ldi r24,0
/* epilogue start */
	pop r29
	pop r28
.LVL19:
	ret
	.cfi_endproc
.LFE11:
	.size	CustomAvrMessageHandler, .-CustomAvrMessageHandler
.global	controller
	.section	.data.controller,"aw",@progbits
	.type	controller, @object
	.size	controller, 5
controller:
	.zero	2
	.byte	-1
	.word	gs(InitialState)
	.text
.Letext0:
	.file 2 "C:\\Users\\sgart\\Documents\\Avr\\ToolChain\\AvrLib\\include/Atmega328P.h"
	.file 3 "c:\\users\\sgart\\documents\\avr\\toolchain\\avr8-gnu-toolchain-win32_x86\\avr\\include\\stdint.h"
	.file 4 "C:\\Users\\sgart\\Documents\\Avr\\ToolChain\\AvrLib\\include/avrlib.h"
	.section	.debug_info,"",@progbits
.Ldebug_info0:
	.long	0x5c8
	.word	0x2
	.long	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.long	.LASF63
	.byte	0xc
	.long	.LASF64
	.long	.Ldebug_ranges0+0
	.long	0
	.long	0
	.long	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.long	.LASF0
	.uleb128 0x3
	.long	.LASF12
	.byte	0x3
	.byte	0x7e
	.long	0x37
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.long	.LASF1
	.uleb128 0x4
	.byte	0x2
	.byte	0x5
	.string	"int"
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.long	.LASF2
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.long	.LASF3
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.long	.LASF4
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.long	.LASF5
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.long	.LASF6
	.uleb128 0x5
	.long	0x2c
	.uleb128 0x6
	.long	.LASF32
	.byte	0x5
	.byte	0x2
	.word	0x114
	.long	0xc6
	.uleb128 0x7
	.long	.LASF7
	.byte	0x2
	.word	0x116
	.long	0x68
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x7
	.long	.LASF8
	.byte	0x2
	.word	0x117
	.long	0x68
	.byte	0x2
	.byte	0x23
	.uleb128 0x1
	.uleb128 0x7
	.long	.LASF9
	.byte	0x2
	.word	0x118
	.long	0x68
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.uleb128 0x7
	.long	.LASF10
	.byte	0x2
	.word	0x119
	.long	0x68
	.byte	0x2
	.byte	0x23
	.uleb128 0x3
	.uleb128 0x7
	.long	.LASF11
	.byte	0x2
	.word	0x11a
	.long	0x68
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x8
	.long	.LASF13
	.byte	0x2
	.word	0x11b
	.long	0x6d
	.uleb128 0x9
	.byte	0x1
	.long	0x37
	.byte	0x4
	.byte	0x1d
	.long	0xeb
	.uleb128 0xa
	.long	.LASF14
	.byte	0
	.uleb128 0xa
	.long	.LASF15
	.byte	0x1
	.byte	0
	.uleb128 0x3
	.long	.LASF16
	.byte	0x4
	.byte	0x20
	.long	0xd2
	.uleb128 0x9
	.byte	0x1
	.long	0x37
	.byte	0x4
	.byte	0x29
	.long	0x10f
	.uleb128 0xa
	.long	.LASF17
	.byte	0
	.uleb128 0xa
	.long	.LASF18
	.byte	0x1
	.byte	0
	.uleb128 0x9
	.byte	0x1
	.long	0x37
	.byte	0x4
	.byte	0x33
	.long	0x134
	.uleb128 0xa
	.long	.LASF19
	.byte	0
	.uleb128 0xa
	.long	.LASF20
	.byte	0x1
	.uleb128 0xa
	.long	.LASF21
	.byte	0x2
	.uleb128 0xa
	.long	.LASF22
	.byte	0x3
	.byte	0
	.uleb128 0x9
	.byte	0x1
	.long	0x37
	.byte	0x4
	.byte	0x6d
	.long	0x171
	.uleb128 0xa
	.long	.LASF23
	.byte	0
	.uleb128 0xa
	.long	.LASF24
	.byte	0xa2
	.uleb128 0xa
	.long	.LASF25
	.byte	0xa3
	.uleb128 0xa
	.long	.LASF26
	.byte	0xa4
	.uleb128 0xa
	.long	.LASF27
	.byte	0xa6
	.uleb128 0xa
	.long	.LASF28
	.byte	0xa8
	.uleb128 0xa
	.long	.LASF29
	.byte	0xa8
	.uleb128 0xa
	.long	.LASF30
	.byte	0xa5
	.byte	0
	.uleb128 0x3
	.long	.LASF31
	.byte	0x4
	.byte	0x76
	.long	0x134
	.uleb128 0xb
	.long	.LASF33
	.byte	0xe
	.byte	0x4
	.byte	0x7d
	.long	0x1b3
	.uleb128 0xc
	.long	.LASF34
	.byte	0x4
	.byte	0x7f
	.long	0x171
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.long	.LASF35
	.byte	0x4
	.byte	0x80
	.long	0x2c
	.byte	0x2
	.byte	0x23
	.uleb128 0x1
	.uleb128 0xc
	.long	.LASF36
	.byte	0x4
	.byte	0x81
	.long	0x1b3
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.byte	0
	.uleb128 0xd
	.long	0x2c
	.long	0x1c3
	.uleb128 0xe
	.long	0x1c3
	.byte	0xb
	.byte	0
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.long	.LASF37
	.uleb128 0x3
	.long	.LASF38
	.byte	0x4
	.byte	0x82
	.long	0x17c
	.uleb128 0xf
	.byte	0x2
	.byte	0x4
	.byte	0x94
	.long	0x1fa
	.uleb128 0xc
	.long	.LASF39
	.byte	0x4
	.byte	0x96
	.long	0x2c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.long	.LASF40
	.byte	0x4
	.byte	0x97
	.long	0x2c
	.byte	0x2
	.byte	0x23
	.uleb128 0x1
	.byte	0
	.uleb128 0x10
	.byte	0x2
	.byte	0x4
	.byte	0x92
	.long	0x213
	.uleb128 0x11
	.long	0x1d5
	.uleb128 0x12
	.string	"Ptr"
	.byte	0x4
	.byte	0x99
	.long	0x213
	.byte	0
	.uleb128 0x13
	.byte	0x2
	.uleb128 0xb
	.long	.LASF41
	.byte	0x6
	.byte	0x4
	.byte	0x8d
	.long	0x253
	.uleb128 0xc
	.long	.LASF42
	.byte	0x4
	.byte	0x8f
	.long	0x2c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x14
	.string	"Id"
	.byte	0x4
	.byte	0x91
	.long	0x2c
	.byte	0x2
	.byte	0x23
	.uleb128 0x1
	.uleb128 0x15
	.long	0x1fa
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.uleb128 0xc
	.long	.LASF43
	.byte	0x4
	.byte	0x9b
	.long	0x253
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x16
	.byte	0x2
	.long	0x215
	.uleb128 0x3
	.long	.LASF44
	.byte	0x4
	.byte	0x9c
	.long	0x215
	.uleb128 0x3
	.long	.LASF45
	.byte	0x4
	.byte	0xa9
	.long	0x26f
	.uleb128 0x16
	.byte	0x2
	.long	0x275
	.uleb128 0x17
	.byte	0x1
	.long	0x281
	.uleb128 0x18
	.long	0x281
	.byte	0
	.uleb128 0x16
	.byte	0x2
	.long	0x287
	.uleb128 0x19
	.long	0x259
	.uleb128 0x16
	.byte	0x2
	.long	0x292
	.uleb128 0x19
	.long	0x1ca
	.uleb128 0xb
	.long	.LASF46
	.byte	0x5
	.byte	0x4
	.byte	0xbd
	.long	0x2ce
	.uleb128 0xc
	.long	.LASF47
	.byte	0x4
	.byte	0xbf
	.long	0x2ce
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xc
	.long	.LASF48
	.byte	0x4
	.byte	0xc0
	.long	0x2c
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.uleb128 0xc
	.long	.LASF49
	.byte	0x4
	.byte	0xc1
	.long	0x264
	.byte	0x2
	.byte	0x23
	.uleb128 0x3
	.byte	0
	.uleb128 0x16
	.byte	0x2
	.long	0x297
	.uleb128 0x1a
	.string	"Fsm"
	.byte	0x4
	.byte	0xc2
	.long	0x297
	.uleb128 0x1b
	.byte	0x1
	.long	.LASF65
	.byte	0x1
	.byte	0x57
	.byte	0x1
	.long	.LFB12
	.long	.LFE12
	.byte	0x3
	.byte	0x92
	.uleb128 0x20
	.sleb128 2
	.byte	0x1
	.long	0x30c
	.uleb128 0x1c
	.string	"msg"
	.byte	0x1
	.byte	0x57
	.long	0x281
	.byte	0x6
	.byte	0x68
	.byte	0x93
	.uleb128 0x1
	.byte	0x69
	.byte	0x93
	.uleb128 0x1
	.byte	0
	.uleb128 0x1d
	.byte	0x1
	.long	.LASF50
	.byte	0x1
	.byte	0x3f
	.long	.LFB9
	.long	.LFE9
	.byte	0x3
	.byte	0x92
	.uleb128 0x20
	.sleb128 2
	.byte	0x1
	.long	0x344
	.uleb128 0x1e
	.long	.LVL1
	.long	0x56c
	.uleb128 0x1f
	.byte	0x1
	.byte	0x68
	.byte	0x1
	.byte	0x32
	.uleb128 0x1f
	.byte	0x1
	.byte	0x66
	.byte	0x1
	.byte	0x33
	.uleb128 0x1f
	.byte	0x1
	.byte	0x64
	.byte	0x1
	.byte	0x31
	.uleb128 0x1f
	.byte	0x1
	.byte	0x62
	.byte	0x1
	.byte	0x32
	.byte	0
	.byte	0
	.uleb128 0x1d
	.byte	0x1
	.long	.LASF51
	.byte	0x1
	.byte	0x46
	.long	.LFB10
	.long	.LFE10
	.byte	0x3
	.byte	0x92
	.uleb128 0x20
	.sleb128 2
	.byte	0x1
	.long	0x37c
	.uleb128 0x1e
	.long	.LVL2
	.long	0x56c
	.uleb128 0x1f
	.byte	0x1
	.byte	0x68
	.byte	0x1
	.byte	0x32
	.uleb128 0x1f
	.byte	0x1
	.byte	0x66
	.byte	0x1
	.byte	0x34
	.uleb128 0x1f
	.byte	0x1
	.byte	0x64
	.byte	0x1
	.byte	0x31
	.uleb128 0x1f
	.byte	0x1
	.byte	0x62
	.byte	0x1
	.byte	0x32
	.byte	0
	.byte	0
	.uleb128 0x20
	.byte	0x1
	.long	.LASF54
	.byte	0x1
	.byte	0x12
	.byte	0x1
	.long	0x3e
	.long	.LFB6
	.long	.LFE6
	.byte	0x3
	.byte	0x92
	.uleb128 0x20
	.sleb128 2
	.byte	0x1
	.long	0x44a
	.uleb128 0x21
	.long	.LVL3
	.long	0x57a
	.long	0x3bc
	.uleb128 0x1f
	.byte	0xc
	.byte	0x66
	.byte	0x93
	.uleb128 0x1
	.byte	0x67
	.byte	0x93
	.uleb128 0x1
	.byte	0x68
	.byte	0x93
	.uleb128 0x1
	.byte	0x69
	.byte	0x93
	.uleb128 0x1
	.byte	0x5
	.byte	0xc
	.long	0x3d090
	.byte	0
	.uleb128 0x21
	.long	.LVL4
	.long	0x588
	.long	0x3d8
	.uleb128 0x1f
	.byte	0x6
	.byte	0x68
	.byte	0x93
	.uleb128 0x1
	.byte	0x69
	.byte	0x93
	.uleb128 0x1
	.byte	0x5
	.byte	0x3
	.long	controller
	.byte	0
	.uleb128 0x21
	.long	.LVL5
	.long	0x596
	.long	0x3f4
	.uleb128 0x1f
	.byte	0x6
	.byte	0x68
	.byte	0x93
	.uleb128 0x1
	.byte	0x69
	.byte	0x93
	.uleb128 0x1
	.byte	0x5
	.byte	0x3
	.long	CustomAvrMessageHandler
	.byte	0
	.uleb128 0x21
	.long	.LVL6
	.long	0x5a3
	.long	0x41a
	.uleb128 0x1f
	.byte	0x1
	.byte	0x68
	.byte	0x1
	.byte	0x30
	.uleb128 0x1f
	.byte	0x1
	.byte	0x66
	.byte	0x1
	.byte	0x33
	.uleb128 0x1f
	.byte	0x6
	.byte	0x64
	.byte	0x93
	.uleb128 0x1
	.byte	0x65
	.byte	0x93
	.uleb128 0x1
	.byte	0x5
	.byte	0x3
	.long	doorOpened
	.byte	0
	.uleb128 0x21
	.long	.LVL7
	.long	0x5a3
	.long	0x440
	.uleb128 0x1f
	.byte	0x1
	.byte	0x68
	.byte	0x1
	.byte	0x31
	.uleb128 0x1f
	.byte	0x1
	.byte	0x66
	.byte	0x1
	.byte	0x33
	.uleb128 0x1f
	.byte	0x6
	.byte	0x64
	.byte	0x93
	.uleb128 0x1
	.byte	0x65
	.byte	0x93
	.uleb128 0x1
	.byte	0x5
	.byte	0x3
	.long	doorClosed
	.byte	0
	.uleb128 0x22
	.long	.LVL8
	.long	0x5b0
	.byte	0
	.uleb128 0x1d
	.byte	0x1
	.long	.LASF52
	.byte	0x1
	.byte	0x29
	.long	.LFB7
	.long	.LFE7
	.byte	0x3
	.byte	0x92
	.uleb128 0x20
	.sleb128 2
	.byte	0x1
	.long	0x4b2
	.uleb128 0x21
	.long	.LVL9
	.long	0x56c
	.long	0x485
	.uleb128 0x1f
	.byte	0x1
	.byte	0x68
	.byte	0x1
	.byte	0x33
	.uleb128 0x1f
	.byte	0x1
	.byte	0x66
	.byte	0x1
	.byte	0x31
	.uleb128 0x1f
	.byte	0x1
	.byte	0x64
	.byte	0x1
	.byte	0x31
	.uleb128 0x1f
	.byte	0x1
	.byte	0x62
	.byte	0x1
	.byte	0x32
	.byte	0
	.uleb128 0x21
	.long	.LVL10
	.long	0x5bd
	.long	0x49d
	.uleb128 0x1f
	.byte	0x1
	.byte	0x68
	.byte	0x1
	.byte	0x30
	.uleb128 0x1f
	.byte	0x1
	.byte	0x66
	.byte	0x1
	.byte	0x30
	.byte	0
	.uleb128 0x1e
	.long	.LVL11
	.long	0x5bd
	.uleb128 0x1f
	.byte	0x1
	.byte	0x68
	.byte	0x1
	.byte	0x31
	.uleb128 0x1f
	.byte	0x1
	.byte	0x66
	.byte	0x1
	.byte	0x31
	.byte	0
	.byte	0
	.uleb128 0x1d
	.byte	0x1
	.long	.LASF53
	.byte	0x1
	.byte	0x33
	.long	.LFB8
	.long	.LFE8
	.byte	0x3
	.byte	0x92
	.uleb128 0x20
	.sleb128 2
	.byte	0x1
	.long	0x51a
	.uleb128 0x21
	.long	.LVL12
	.long	0x56c
	.long	0x4ed
	.uleb128 0x1f
	.byte	0x1
	.byte	0x68
	.byte	0x1
	.byte	0x33
	.uleb128 0x1f
	.byte	0x1
	.byte	0x66
	.byte	0x1
	.byte	0x32
	.uleb128 0x1f
	.byte	0x1
	.byte	0x64
	.byte	0x1
	.byte	0x31
	.uleb128 0x1f
	.byte	0x1
	.byte	0x62
	.byte	0x1
	.byte	0x32
	.byte	0
	.uleb128 0x21
	.long	.LVL13
	.long	0x5bd
	.long	0x505
	.uleb128 0x1f
	.byte	0x1
	.byte	0x68
	.byte	0x1
	.byte	0x30
	.uleb128 0x1f
	.byte	0x1
	.byte	0x66
	.byte	0x1
	.byte	0x31
	.byte	0
	.uleb128 0x1e
	.long	.LVL14
	.long	0x5bd
	.uleb128 0x1f
	.byte	0x1
	.byte	0x68
	.byte	0x1
	.byte	0x31
	.uleb128 0x1f
	.byte	0x1
	.byte	0x66
	.byte	0x1
	.byte	0x30
	.byte	0
	.byte	0
	.uleb128 0x23
	.byte	0x1
	.long	.LASF55
	.byte	0x1
	.byte	0x4e
	.byte	0x1
	.long	0xeb
	.long	.LFB11
	.long	.LFE11
	.long	.LLST0
	.byte	0x1
	.long	0x55a
	.uleb128 0x24
	.string	"msg"
	.byte	0x1
	.byte	0x4e
	.long	0x28c
	.long	.LLST1
	.uleb128 0x22
	.long	.LVL17
	.long	0x44a
	.uleb128 0x22
	.long	.LVL18
	.long	0x4b2
	.byte	0
	.uleb128 0x25
	.long	.LASF66
	.byte	0x1
	.byte	0x10
	.long	0x2d4
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.long	controller
	.uleb128 0x26
	.byte	0x1
	.byte	0x1
	.long	.LASF56
	.long	.LASF56
	.byte	0x4
	.word	0x154
	.uleb128 0x26
	.byte	0x1
	.byte	0x1
	.long	.LASF57
	.long	.LASF57
	.byte	0x4
	.word	0x163
	.uleb128 0x26
	.byte	0x1
	.byte	0x1
	.long	.LASF58
	.long	.LASF58
	.byte	0x4
	.word	0x141
	.uleb128 0x27
	.byte	0x1
	.byte	0x1
	.long	.LASF59
	.long	.LASF59
	.byte	0x4
	.byte	0xe4
	.uleb128 0x27
	.byte	0x1
	.byte	0x1
	.long	.LASF60
	.long	.LASF60
	.byte	0x4
	.byte	0xfc
	.uleb128 0x27
	.byte	0x1
	.byte	0x1
	.long	.LASF61
	.long	.LASF61
	.byte	0x4
	.byte	0xd8
	.uleb128 0x26
	.byte	0x1
	.byte	0x1
	.long	.LASF62
	.long	.LASF62
	.byte	0x4
	.word	0x104
	.byte	0
	.section	.debug_abbrev,"",@progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x4
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0xd
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0xd
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.uleb128 0x2117
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.uleb128 0x2117
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x410a
	.byte	0
	.uleb128 0x2
	.uleb128 0xa
	.uleb128 0x2111
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.uleb128 0x2117
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x21
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x22
	.uleb128 0x4109
	.byte	0
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x23
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x2117
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x24
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x25
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x26
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.uleb128 0x2007
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0x27
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.uleb128 0x2007
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",@progbits
.Ldebug_loc0:
.LLST0:
	.long	.LFB11
	.long	.LCFI0
	.word	0x3
	.byte	0x92
	.uleb128 0x20
	.sleb128 2
	.long	.LCFI0
	.long	.LCFI1
	.word	0x3
	.byte	0x92
	.uleb128 0x20
	.sleb128 3
	.long	.LCFI1
	.long	.LFE11
	.word	0x3
	.byte	0x92
	.uleb128 0x20
	.sleb128 4
	.long	0
	.long	0
.LLST1:
	.long	.LVL15
	.long	.LVL16
	.word	0x6
	.byte	0x68
	.byte	0x93
	.uleb128 0x1
	.byte	0x69
	.byte	0x93
	.uleb128 0x1
	.long	.LVL16
	.long	.LVL19
	.word	0x6
	.byte	0x6c
	.byte	0x93
	.uleb128 0x1
	.byte	0x6d
	.byte	0x93
	.uleb128 0x1
	.long	.LVL19
	.long	.LFE11
	.word	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x68
	.byte	0x9f
	.long	0
	.long	0
	.section	.debug_aranges,"",@progbits
	.long	0x4c
	.word	0x2
	.long	.Ldebug_info0
	.byte	0x4
	.byte	0
	.word	0
	.word	0
	.long	.LFB12
	.long	.LFE12-.LFB12
	.long	.LFB9
	.long	.LFE9-.LFB9
	.long	.LFB10
	.long	.LFE10-.LFB10
	.long	.LFB6
	.long	.LFE6-.LFB6
	.long	.LFB7
	.long	.LFE7-.LFB7
	.long	.LFB8
	.long	.LFE8-.LFB8
	.long	.LFB11
	.long	.LFE11-.LFB11
	.long	0
	.long	0
	.section	.debug_ranges,"",@progbits
.Ldebug_ranges0:
	.long	.LFB12
	.long	.LFE12
	.long	.LFB9
	.long	.LFE9
	.long	.LFB10
	.long	.LFE10
	.long	.LFB6
	.long	.LFE6
	.long	.LFB7
	.long	.LFE7
	.long	.LFB8
	.long	.LFE8
	.long	.LFB11
	.long	.LFE11
	.long	0
	.long	0
	.section	.debug_line,"",@progbits
.Ldebug_line0:
	.section	.debug_str,"MS",@progbits,1
.LASF46:
	.string	"Fsm_tag"
.LASF58:
	.string	"RegisterFsm"
.LASF50:
	.string	"doorClosed"
.LASF6:
	.string	"long long unsigned int"
.LASF43:
	.string	"__next"
.LASF17:
	.string	"ExtInterruptSource0"
.LASF18:
	.string	"ExtInterruptSource1"
.LASF5:
	.string	"long long int"
.LASF0:
	.string	"signed char"
.LASF60:
	.string	"RegisterExternalInteruptHandler"
.LASF25:
	.string	"PacketType_TestCommand"
.LASF3:
	.string	"long int"
.LASF15:
	.string	"True"
.LASF27:
	.string	"PacketType_ReadRegister"
.LASF63:
	.string	"GNU C99 5.4.0 -mn-flash=1 -mno-skip-bug -mmcu=avr5 -g2 -O1 -std=gnu99 -funsigned-char -funsigned-bitfields -ffunction-sections -fdata-sections -fpack-struct -fshort-enums"
.LASF51:
	.string	"doorOpened"
.LASF38:
	.string	"AvrMessage"
.LASF22:
	.string	"ExtIntTrigger_OnRaisingEdge"
.LASF19:
	.string	"ExtIntTrigger_OnLow"
.LASF52:
	.string	"openDoor"
.LASF41:
	.string	"Message_tag"
.LASF28:
	.string	"PacketType_WriteRegister"
.LASF65:
	.string	"InitialState"
.LASF2:
	.string	"unsigned int"
.LASF23:
	.string	"PacketType_Undefined"
.LASF4:
	.string	"long unsigned int"
.LASF55:
	.string	"CustomAvrMessageHandler"
.LASF49:
	.string	"CurrentState"
.LASF13:
	.string	"TCNT8_T"
.LASF34:
	.string	"MsgType"
.LASF30:
	.string	"PacketType_TraceMessage"
.LASF24:
	.string	"PacketType_LiftSimulatorButton"
.LASF32:
	.string	"TCNT8_T_tag"
.LASF26:
	.string	"PacketType_RawData"
.LASF47:
	.string	"Next"
.LASF20:
	.string	"ExtIntTrigger_OnChange"
.LASF37:
	.string	"sizetype"
.LASF9:
	.string	"TCNT"
.LASF36:
	.string	"Payload"
.LASF14:
	.string	"False"
.LASF10:
	.string	"OCRA"
.LASF11:
	.string	"OCRB"
.LASF62:
	.string	"SetExtInterruptEnable"
.LASF40:
	.string	"MsgParamHigh"
.LASF29:
	.string	"PacketType_TraceMassagePadLen"
.LASF1:
	.string	"unsigned char"
.LASF7:
	.string	"TCCRA"
.LASF8:
	.string	"TCCRB"
.LASF31:
	.string	"AvrPacketType"
.LASF48:
	.string	"RxMask"
.LASF64:
	.string	"C:\\Users\\sgart\\Documents\\Coding\\C\\cpp\\lift-door\\Build\\HelloWorld.c"
.LASF59:
	.string	"RegisterAvrMessageHandler"
.LASF56:
	.string	"SendMessage"
.LASF44:
	.string	"Message"
.LASF57:
	.string	"Usart_Init"
.LASF61:
	.string	"InitializeStateEventFramework"
.LASF35:
	.string	"Length"
.LASF53:
	.string	"closeDoor"
.LASF33:
	.string	"AvrMessage_tag"
.LASF42:
	.string	"Priority"
.LASF12:
	.string	"uint8_t"
.LASF45:
	.string	"StateHandler"
.LASF39:
	.string	"MsgParamLow"
.LASF21:
	.string	"ExtIntTrigger_OnFallingEdge"
.LASF16:
	.string	"Bool"
.LASF54:
	.string	"main"
.LASF66:
	.string	"controller"
	.ident	"GCC: (AVR_8_bit_GNU_Toolchain_3.6.2_1759) 5.4.0"
.global __do_copy_data
