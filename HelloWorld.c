#include <Atmega328P.h>
#include <avrlib.h>
#include <RegisterAccess.h>
#define F_CPU 16000000
#include <util/delay.h>

#define Port_5_mask 0x20;

void openDoor();
void CloseDoor();
void doorClosed();
void doorOpened();
Bool CustomAvrMessageHandler(const AvrMessage* msg);
void InitialState(const Message*msg);

Fsm controller = {.RxMask=0xFF, .CurrentState=InitialState};

int main(void) {
	Usart_Init(250000);

    // register fsm
    RegisterFsm(&controller);

    RegisterAvrMessageHandler(CustomAvrMessageHandler);
    
    // register Interrupts
    RegisterExternalInteruptHandler(ExtInterruptSource0, ExtIntTrigger_OnRaisingEdge, doorOpened);
    RegisterExternalInteruptHandler(ExtInterruptSource1, ExtIntTrigger_OnRaisingEdge, doorClosed);

    // PortD Pin5,6
    Tcnt0.TCCRA = 1 | (2 << 4) | (2 << 6);
    Tcnt0.TCCRB = 0x5;
    Tcnt0.OCRA = 0;
    Tcnt0.OCRB = 0;

    InitializeStateEventFramework();
    return 0;
}

// Start moving
void openDoor() {
    SendMessage(3, 1, 0x1, 0x2);

    // turn on sensor for open and turn off sensor for close
    SetExtInterruptEnable(ExtInterruptSource0, False);
    SetExtInterruptEnable(ExtInterruptSource1, True);

    Tcnt0.OCRA = 0x33; // more or less 20%
    Tcnt0.OCRB = 0x0;
}
void closeDoor() {
    SendMessage(3, 2, 0x1, 0x2);

    // turn off sensor for open and turn on sensor for close
    SetExtInterruptEnable(ExtInterruptSource0, True);
    SetExtInterruptEnable(ExtInterruptSource1, False);

    Tcnt0.OCRA = 0x0;
    Tcnt0.OCRB = 0x33; // more or less 20%
}

// Stop moving
void doorClosed() {
    SendMessage(2, 3, 0x1, 0x2);

    // stop door movement
    Tcnt0.OCRA = 0x0;
    Tcnt0.OCRB = 0x0;
}
void doorOpened() {
    SendMessage(2, 4, 0x1, 0x2);

    // stop door movement
    Tcnt0.OCRA = 0x0;
    Tcnt0.OCRB = 0x0;
}

Bool CustomAvrMessageHandler(const AvrMessage* msg) {
    // if ( DefaultMessageHandler(msg) ) return True; // does NOT compile

    if (msg->Payload[0] == 0xcf) openDoor();
    if (msg->Payload[0] == 0xef) closeDoor();

    return False;
}

void InitialState(const Message*msg){
    
}

